#! /bin/sh
# Begin xrdp
echo "This will PURGE xRDP, reinstall xRDP, and configure it for 14.04 or 18.04 (WIP)." 
echo "Press ENTER to procceed or CTRL+C to ABORT." ; read bleh  
echo "OK. ALL AHEAD FULL!"
sleep .5
sudo apt-get purge xrdp

if [[ -x /usr/sbin/ntpdate ]]; then
	echo "[INFO] No installs needed."
else
	echo ; aptsl=$(cat /etc/apt/sources.list |grep -c "\#\#deb")
	if [[ $aptsl != 0 ]]; then
		sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak ; sudo sed -i 's/^##*deb/deb/' /etc/apt/sources.list
		#echo "[INFO] sources uncommented" >> "${vf}"
	fi
	echo "[INFO] Please wait. (Updating - can take up to 5 minutes)"
	sudo apt-get update -qq ; echo "[INFO] Update done. Any errors above can usually be ignored." ; echo
	echo "[INFO] Please wait. (Installing - can take up to 30 minutes)"
	sudo apt-get install -qq -y xfce4 xfce4-terminal gnome-icon-theme-full tango-icon-theme curl ntpdate gvncviewer xrdp ntp #2>&1 >/dev/null
	echo "[INFO] Update done. Configuring XRDP" ; echo xfce4-session >~/.xsession
	sleep 1
	
	# ToDo: please detect 14.04 or 18.04
	
	# For 14.04 (Should work no problem, please test)
	sudo sed -i 's/\. \/etc\/X11\/Xsession/startxfce4/' /etc/xrdp/startwm.sh
	
	# For 18.04
	# ToDo: Comment out 'exec' lines and append 'gnome-session' to the end 
	#sudo sed -i 's/\. \/etc\/gnome-session/' /etc/xrdp/startwm.sh
	
	echo ; sleep 1 ; sudo service xrdp restart ; vncserver
	for (( i=1; i <= 5; i++ )); do
		vncserver -kill :$i #2>&1 >/dev/null
		done
	# Breaks here on some legacy systems
	sudo cp ~/.vnc/xstartup ~/.vnc/xstartup.bak
	sed -i 's/# u/u/' ~/.vnc/xstartup
	sed -i 's/# e/exec \/etc\/X11\/Xsession #/' ~/.vnc/xstartup
	echo "[INFO] XRDP configured. Now would be a good time to remote in, click the default button and disable the screensaver." ; vncserver
fi
